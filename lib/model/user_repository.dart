import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import '../network_util.dart';
import '../user.dart';

class UserRepository with ChangeNotifier {

  NetworkUtil _netUtil = new NetworkUtil();
  static final BASE_URL = "http://157.245.97.187:9090";
  static final SIGNUPURL = BASE_URL + "/v1/user/signin";
  static final Contest = BASE_URL + "/v1/contest";



  UserRepository.instance();

  Future<String> getContest() async {
    var res = await http.get(Uri.encodeFull(Contest),
        headers: {
          "Accept": "application/json",
          "content-type": "application/json"
        });

    var resBody = json.decode(res.body);

    return resBody["Data"]["Manual"][0].toString();
  }


  Future<User> login(String username, String password) {
    return _netUtil.post(SIGNUPURL,
        body: {"Mobile": username, "Password": password}).then((dynamic res) {
      if (res["Status"] == "Success") {

        print(res);
        Fluttertoast.showToast(
            msg: res["Status"].toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white);
      }
      if (res["Status"] == "Failure") {
        var Error = res["Error"];
        Fluttertoast.showToast(
            msg: Error["Message"].toString(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white);
        throw new Exception(res);
      }
      return new User.map(res["Data"]);
    });
  }


}
